var express = require('express');
var router = express.Router();
var axios = require('axios');
var moment = require('moment');

const publicOfertaEducativa = [
'Administración',
'Administración de Empresas Turísticas',
'Administración de Negocios',
'Administración de Recursos Humanos',
'Administración de Tecnologías de Información',
'Administración de Ventas',
'Administración y Finanzas',
'Ciencias Políticas y Administración Pública',
'Comunicación',
'Comunicación Digital',
'Comunicación Organizacional',
'Contaduría Pública',
'Contaduría y Finanzas',
'Desarrollo Sustentable y Ecoturismo',
'Ingeniería Sistemas Computacionales',
'Ingeniería Industrial',
'Ingeniería Industrial y Administración',
'Mercadotecnia',
'Negocios Internacionales',
'Pedagogía',
'Psicología Organizacional',
'Criminología y Criminalística',
'Maestría: Ciencia de Datos para Negocios',
'Maestría: Administración de Tecnologías de Información',
'Maestría: Dirección e Ingeniería de Software',
'Maestría: Ingeniería y Tecnología Ambiental',
'Maestría: Dirección de Proyectos de Innovación',
'Maestría: Ciencias Computacionales y Telecomunicaciones',
'Maestría: Administración de Negocios',
'Maestría: Marketing Digital y E-Commerce',
'Maestría: Administración de Instituciones Educativas',
'Maestría: Administración en Marketing Estratégico',
'Maestría: Administración de Negocios Deportivos',
'Maestría: Administración de Recursos Humanos',
'Maestría: Administración Pública',
'Maestría: Coaching Integral y Organizacional',
'Maestría: Dirección de Ventas',
'Maestría: Gestión Estratégica del Capital Humano',
'Maestría: Comercio Internacional',
'Maestría: Educación y Docencia',
'Maestría: Gestión Directiva en Salud',
'Maestría: Dirección de Empresas Turísticas',
'Maestría: Dirección de Negocios de Alimentos y Bebidas',
'Maestría: Gestión Organizacional Positiva',
'Maestría: Mindfulness',
'Maestría: Administración Pública Colaboración CIFAL Málaga',
'Doctorado: Administración Estratégica Empresarial',
'Doctorado: Educación',
]

const sendOfertaEducativa = [
'Carrera, Administraci%f3n',
'Carrera, Administraci%f3n de Empresas Tur%edsticas',
'Carrera, Administraci%f3n de Negocios',
'Carrera, Administraci%f3n de Recursos Humanos',
'Carrera, Administraci%f3n de Tecnolog%edas de Informaci%f3n',
'Carrera, Administraci%f3n de Ventas',
'Carrera, Administraci%f3n y Finanzas',
'Carrera, Ciencias Pol%edticas y Administraci%f3n P%fablica',
'Carrera, Comunicaci%f3n',
'Carrera, Comunicaci%f3n Digital',
'Carrera, Comunicaci%f3n Organizacional',
'Carrera, Contadur%eda P%fablica',
'Carrera, Contadur%eda y Finanzas',
'Carrera, Desarrollo Sustentable y Ecoturismo',
'Carrera, Ingenier%eda Sistemas Computacionales',
'Carrera, Ingenier%eda Industrial',
'Carrera, Ingenier%eda Industrial y Administraci%f3n',
'Carrera, Mercadotecnia',
'Carrera, Negocios Internacionales',
'Carrera, Pedagog%eda',
'Carrera, Psicolog%eda Organizacional',
'Carrera, Criminolog%eda y Criminal%edstica',
'Maestr%edas, Ciencia de Datos para Negocios',
'Maestr%edas, Administraci%f3n de Tecnolog%edas de Informaci%f3n',
'Maestr%edas, Direcci%f3n e Ingenier%eda de Software',
'Maestr%edas, Ingenier%eda y Tecnolog%eda Ambiental',
'Maestr%edas, Direcci%f3n de Proyectos de Innovaci%f3n',
'Maestr%edas, Ciencias Computacionales y Telecomunicaciones',
'Maestr%edas, Administraci%f3n de Negocios',
'Maestr%edas, Marketing Digital y E-Commerce',
'Maestr%edas, Administraci%f3n de Instituciones Educativas',
'Maestr%edas, Administraci%f3n en Marketing Estrat%e9gico',
'Maestr%edas, Administraci%f3n de Negocios Deportivos',
'Maestr%edas, Administraci%f3n de Recursos Humanos',
'Maestr%edas, Administraci%f3n P%fablica',
'Maestr%edas, Coaching Integral y Organizacional',
'Maestr%edas, Direcci%f3n de Ventas',
'Maestr%edas, Gesti%f3n Estrat%e9gica del Capital Humano',
'Maestr%edas, Comercio Internacional',
'Maestr%edas, Educaci%f3n y Docencia',
'Maestr%edas, Gesti%f3n Directiva en Salud',
'Maestr%edas, Direcci%f3n de Empresas Tur%edsticas',
'Maestr%edas, Direcci%f3n de Negocios de Alimentos y Bebidas',
'Maestr%edas, Gesti%f3n Organizacional Positiva',
'Maestr%edas, Mindfulness',
'Maestr%edas, Administraci%f3n P%fablica Colaboraci%f3n CIFAL M%e0laga',
'Doctorado, Administraci%f3n Estrat%e9gica Empresarial',
'Doctorado, Educaci%f3n',
]


/* GET home page. */
router.get('/', function(req, res, next) {
  console.log(req.query)
let email= req.query.email;
let telefono_fijo = req.query.telefono_fijo;
let apellido_materno = req.query.apellido_materno;
let apellido_paterno = req.query.apellido_paterno;
let celular = req.query.celular;
let comentarios = req.query.comentarios;
let date_transaction = req.query.created_time ? req.query.created_time.replace(/\//g,'-') : '';//.format('dd-mm-aaaa hh:mm:ss');
let estado = req.query.estado;
let lada = req.query.lada;
let marca = req.query.marca;
let nombre = req.query.nombre; 
let pais = req.query.pais;
let promocion = req.query.promocion;
let promotor = req.query.promotor;
let transaction_id = req.query.id;

date_transaction = date_transaction + ':00'
//elimina el signo de + y espacios
if(celular){
  celular = celular.replace('+','');
  celular = celular.replace(' ','');
}else{
  celular='00';
}
//si es mayor elimina los primeros digitos si es menor se agregan
while (celular.length != 10) {
  celular = celular.length > 10 ? celular.substr(1) : '0'.concat(celular);
}

/*si se envian otras carreras este se vera modificado*/
let estudios_de_interes
if(publicOfertaEducativa.indexOf(req.query.estudios_de_interes)>-1){
  estudios_de_interes= sendOfertaEducativa[publicOfertaEducativa.indexOf(req.query.estudios_de_interes)];
}else{
  estudios_de_interes = req.query.estudios_de_interes;
}

estudios_de_interes = estudios_de_interes ? estudios_de_interes : req.query.ad_id;
//console.log('date!!', req.query.created_time)
console.log('estudios de interes ', estudios_de_interes)

console.log('Peticion', `http://162.209.79.96/colector.php?email=${email}
&telefono_fijo=${telefono_fijo}
&apellido_materno=${apellido_materno}
&apellido_paterno=${apellido_paterno}
&celular=${celular}
&comentarios=${comentarios}
&date_transaction=${date_transaction}
&estado=${estado}
&estudios_de_interes=${estudios_de_interes}
&lada=${lada}
&marca=${marca}
&nombre=${nombre}
&pais=${pais}
&promocion=${promocion}
&promotor=${promotor}
&transaction_id=${transaction_id}`);
  
  axios.get(encodeURI(`http://162.209.79.96/colector.php?email=${email}&telefono_fijo=${telefono_fijo}&apellido_materno=${apellido_materno}&apellido_paterno=${apellido_paterno}&celular=${celular}&comentarios=${comentarios}&date_transaction=${date_transaction}&estado=${estado}&estudios_de_interes=${estudios_de_interes}&lada=${lada}&marca=${marca}&nombre=${nombre}&pais=${pais}&promocion=${promocion}&promotor=${promotor}&transaction_id=${transaction_id}`))
  .then((response) => {

    console.log(response.data);
    //console.log(response.status);
   // console.log(response.statusText);
   // console.log(response.headers);
    //console.log(response.config);

    res.send(response.data);
  });
 
 // res.send(celular);
  
 
  //res.render('index', { title: 'Express' });
});

module.exports = router;

//Carrera, Administración de Empresas Turísticas

// /?id=632091201003672&created_time=15%2F10%2F2020+20%3A43&campaign_id=202009_UTEL_Peru_Leadads&adset_id=Lookalike2_Licenciaturas&ad_id=Unp2_Lic&
//email=nat.estrada%40gmail.com&platform=fb&promotor=juliusperu&pais=peru&marca=utelp&estado=na&comentarios=na&telefono_fijo=na&lada=na&apellido_materno=na&
//promocion=display&estudios_de_interes=Carrera%2C+Criminolog%C3%ADa+y+Criminal%C3%ADstica&nombre=Natalia&apellido_paterno=Estrada&celular=%2B528712346749